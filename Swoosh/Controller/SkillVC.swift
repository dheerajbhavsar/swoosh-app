//
//  SkillVC.swift
//  Swoosh
//
//  Created by DHEERAJ BHAVSAR on 26/01/18.
//  Copyright © 2018 Dheeraj Bhavsar. All rights reserved.
//

import UIKit

class SkillVC: UIViewController {
    
    var player: Player!

    @IBOutlet var skillButtons: [BorderButton]!
    @IBOutlet weak var finishButton: BorderButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func skillSelectionButtonTapped(_ sender: BorderButton) {
        selectSkill(skillType: sender.currentTitle!)
        for i in 0..<skillButtons.count {
            let button = skillButtons[i]
            button.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4303831336)
            button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
        sender.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        sender.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    func selectSkill(skillType: String) {
        player.selectedSkillLevel = skillType
        finishButton.isEnabled = true
    }
    

    @IBAction func finishButtonTapped(_ sender: BorderButton) {
        performSegue(withIdentifier: "finishVCSegue", sender: self)
    }
    
    @IBAction func unwindFromFinishVCToSkillVC(unwindSegue: UIStoryboardSegue) {
    }
    
//     MARK: - Navigation
//
//     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let finishVC = segue.destination as? FinishVC {
            finishVC.player = player
        }
//         Get the new view controller using segue.destinationViewController.
//         Pass the selected object to the new view controller.
    }
 

}
