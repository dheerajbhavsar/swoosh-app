//
//  FinishVC.swift
//  Swoosh
//
//  Created by DHEERAJ BHAVSAR on 30/01/18.
//  Copyright © 2018 Dheeraj Bhavsar. All rights reserved.
//

import UIKit

class FinishVC: UIViewController {
    
    var player: Player!
    
    @IBOutlet weak var leagueLabel: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        leagueLabel.text = player.desiredLeague
        skillLabel.text = player.selectedSkillLevel

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
