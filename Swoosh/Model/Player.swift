//
//  Player.swift
//  Swoosh
//
//  Created by DHEERAJ BHAVSAR on 28/01/18.
//  Copyright © 2018 Dheeraj Bhavsar. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String!
    var selectedSkillLevel: String!
}
